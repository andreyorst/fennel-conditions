(local lua-error error)
(local lib-name (or ... :condition-system))

(eval-compiler
  (local lib-name (or ... :condition-system))
  (local impl (.. lib-name :-impl))
  (local utils (.. lib-name :-utils))

  ;; Utility functions

  (fn vararg? []
    ;; Check if ... is in in scope.
    (. (get-scope) :vararg))

  (fn current-scope []
    ;; A getter for current dynamic scope
    (let [_ (sym :_)
          scope (gensym :scope)]
      `(let [utils# (require ,utils)
             thread# (or (and coroutine
                              coroutine.running
                              (tostring (coroutine.running)))
                         :main)]
         (match (. utils# :dynamic-scope thread#)
           ,scope ,scope
           ,_ (let [,scope {:handlers {}
                            :restarts {}}]
                (tset utils# :dynamic-scope thread# ,scope)
                ,scope)))))

  (fn function-form? [form]
    ;; Check if `form` evaluates to a function
    (when (list? form)
      (let [[f name? arglist] form]
        (if (or (= 'fn f) (= 'lambda f) (= 'λ f))
            (if (sym? name?)
                (and (sequence? arglist) true)
                (and (sequence? name?) true))
            (= 'hashfn f)))))

  (fn seq-to-table [seq]
    ;; Transform binding sequence into associative table.
    ;; [:a 1 :b 2] => {:a 1 :b 2}
    (let [tbl {}]
      (for [i 1 seq.n 2]
        (tset tbl (. seq i) (. seq (+ i 1))))
      tbl))

  ;; Condition library public API macros

  (fn handler-bind [binding-vec ...]
    "Bind handlers to conditions.

`binding-vec' is a sequential table of conditions and their respecting
handlers, followed by the body expression.  Each handler should be a
function of at least one argument - the condition being handled.
Other arguments are optional, and can be used inside the handler.

If the body expression or any of its subsequent expressions raises a
condition, and a handler is bound for this condition type, the handler
function is invoked.  If no handler were bound, handlers are searched
up the dynamic scope. If no handler found, condition is thrown as a
Lua error.

If invoked handler exits normally, a condition is re-raised.  To
prevent re-raising, use `invoke-restart' function.

# Examples

Handlers executed but their return values are not used:

``` fennel
(var res nil)

(assert-not
 (handler-bind [:signal-condition (fn [] (set res \"signal\") 10)
                :error-condition (fn [] (set res \"error\") 20)]
   (signal :error-condition)))

(assert-eq res :error)
```

To provide a return value use either `handler-case' or a combination
of `restart-case' and `invoke-restart'."
    ;; This will be a common pattern for `handler-bind`, `restart-case`,
    ;; and `handler-case`.  A lot of stuff happens at compile time, and
    ;; dynamic scope is constructed at compile time to avoid explicit
    ;; iteration.  Because of it most variables are defined using
    ;; `gensym` to be accessible at compile time to build code, and at
    ;; runtime to access that code.
    (let [target (gensym :target)
          scope (gensym :scope)
          binding-len (length binding-vec)
          setup '(do)]
      (assert-compile (= (% binding-len 2) 0)
                      "expected even number of signal/handler bindings"
                      binding-vec)
      ;; check each handler to be a symbol or a function definition, and
      ;; put a handler into dynamic scope constructor stored in `setup`
      (for [i binding-len 1 -2]
        (let [condition-object (. binding-vec (- i 1))
              handler (. binding-vec i)]
          (assert-compile (or (sym? handler) (function-form? handler))
                          "handler must be a function"
                          handler)
          (table.insert setup `(assert (not= nil ,condition-object)
                                       "condition object must not be nil"))
          (table.insert
           setup
           `(tset ,scope :handlers {:parent (. ,scope :handlers)
                                    :target ,target
                                    :handler-type :handler-bind
                                    :handler {,condition-object
                                              ,handler}}))))
      `(let [,target {}
             {:pack pack#} (require ,utils)
             {:pcall-handler-bind pcall-handler-bind#} (require ,impl)
             ,scope ,(current-scope)
             orig-handlers# (. ,scope :handlers)]
         ,setup
         ,(if (vararg?)
              `(pcall-handler-bind#
                (fn [...] (pack# (do ,...))) ,scope ,target orig-handlers# ...)
              `(pcall-handler-bind#
                (fn [] (pack# (do ,...))) ,scope ,target orig-handlers#)))))

  (fn restart-case [expr ...]
    "Condition restart point.

Accepts expression `expr' and restarts that can be used when handling
conditions thrown from within the expression.  Similarly to
`handler-case' restarts are lists with first element being a restart
name, and an fn-tail.

If expression or any of it's subsequent expressions raises a
condition, it will be possible to return into the `restart-case', and
execute one of the provided restarts.  Restarts can be executed with
the `invoke-restart' function from handlers bound with `handler-bind'.
Restart names are always strings.

# Examples

Specifying two restarts for `:signal-condition`:

``` fennel
(restart-case (signal :signal-condition)
  (:some-restart [] :body)
  (:some-other-restart [] :other-body))
```"
    (let [target (gensym :target)
          scope (gensym :scope)
          restarts (table.pack ...)
          setup '(do)]
      (for [i restarts.n 1 -1]
        (let [[restart & [arglist &as fn-tail]] (. restarts i)]
          (assert-compile (list? (. restarts i)) "restarts must be defined as lists" restart)
          (assert-compile (or (sequence? arglist)) "expected parameter table" restart)
          (assert-compile (or (= :string (type restart))) "restart name must be a string literal" restart)
          (let [[args descr body] fn-tail
                restart {:restart (list 'fn (unpack fn-tail))
                         :name restart
                         :description (when (and (= :string (type descr))
                                                 (not= nil body))
                                        descr)
                         :args (when (not= nil (next args))
                                 (icollect [_ v (ipairs args)]
                                   (view v {:one-line? true})))}]
            (table.insert setup `(tset ,scope :restarts {:parent (. ,scope :restarts)
                                                         :target ,target
                                                         :restart ,restart})))))
      `(let [,target {}
             {:pack pack#} (require ,utils)
             {:pcall-restart-case pcall-restart-case#} (require ,impl)
             ,scope ,(current-scope)
             orig-restarts# (. ,scope :restarts)]
         ,setup
         ,(if (vararg?)
              `(pcall-restart-case#
                (fn [...] (pack# (do ,expr))) ,scope ,target orig-restarts# ...)
              `(pcall-restart-case#
                (fn [] (pack# (do ,expr))) ,scope ,target orig-restarts#)))))

  (fn handler-case [expr ...]
    "Condition handling point, similar to try/catch.

Accepts expression `expr' and handlers that can be used to handle
conditions, raised from within the expression or any subsequent
expressions.  Provides the facility to catch named conditions raised
with `signal', `warn` and `error' functions.  If any condition is
raised, before propagating condition to error, a handler is searched.
If handler is bound for this condition, it is executed, and the result
of `handler-case' expression will be the result of the handler.

Handlers are defined as lists, where the first object represents the
condition to handle, and the rest is fn-tail - a sequential table of
function arguments, followed by the function body.

# Examples

Handling `error' condition:

``` fennel
(assert-eq 42 (handler-case (error :error-condition)
                (:error-condition [] 42)))
```"
    (let [target (gensym :target)
          scope (gensym :scope)
          handlers (table.pack ...)
          setup `(do)]
      (for [i handlers.n 1 -1]
        (let [[condition-object arglist &as handler] (. handlers i)]
          (assert-compile (list? handler) "handlers must be defined as lists" handler)
          (assert-compile (sequence? arglist) "expected parameter table" handler)
          (table.insert setup `(assert (not= nil ,condition-object)
                                       "condition object must not be nil"))
          (table.insert setup `(tset ,scope :handlers {:parent (. ,scope :handlers)
                                                       :target ,target
                                                       :handler-type :handler-case
                                                       :handler {,condition-object ,(list 'fn (unpack handler 2))}}))))
      `(let [,target {}
             {:pack pack#} (require ,utils)
             {:pcall-handler-case pcall-handler-case#} (require ,impl)
             ,scope ,(current-scope)
             orig-handlers# (. ,scope :handlers)]
         ,setup
         ,(if (vararg?)
              `(pcall-handler-case#
                (fn [...] (pack# (do ,expr))) ,scope ,target orig-handlers# ...)
              `(pcall-handler-case#
                (fn [] (pack# (do ,expr))) ,scope ,target orig-handlers#)))))

  (fn define-condition [condition-symbol ...]
    "Create base condition object with `condition-symbol' from which
conditions can later be derived with `make-condition'.  Accepts
additional `:parent` and `:name` key value pairs.  If no `:name`
specified, uses `condition-symbol`'s `tostring` representation.  If no
`:parent` given uses `Condition' object as a parent.

# Examples

Altering condition's printable name:

``` fennel
(define-condition dbz :name :divide-by-zero)
```

Creating `math-error` condition with parent set to `Error` condition,
and `divide-by-zero` condition with parent set to `math-error`, and handling it:

``` fennel
(define-condition math-error :parent Error)
(define-condition divide-by-zero :parent math-error)

(assert-eq :ok (handler-case (error divide-by-zero)
                 (math-error [] :ok)))
```"
    (assert-compile (sym? condition-symbol) "condition-object must be a symbol" condition-symbol)
    (let [allowed-options {:parent true :name true}
          options (seq-to-table (table.pack ...))
          condition-object {:name (tostring condition-symbol) :type :condition}]
      (each [k (pairs options)]
        (assert-compile (. allowed-options k) (.. "invalid key: " (tostring k)) k))
      (each [k (pairs allowed-options)]
        (match (. options k)
          v (tset condition-object k v)))
      (when (= nil condition-object.parent)
        (tset condition-object :parent `(. (require ,impl) :Condition)))
      `(local ,condition-symbol (let [{:condition= eq#} (require ,impl)
                                      condition-object# ,condition-object
                                      name# condition-object#.name]
                                  (doto condition-object#
                                    (setmetatable {:__eq eq#
                                                   :__name (.. "condition " name#)
                                                   :__fennelview #(.. "#<" (tostring condition-object#) ">")})
                                    (tset :id condition-object#))))))

  (fn cerror [continue-description condition-object ...]
    "Raise `condition-object' as an error with auto-bound `:continue` restart, described by `continue-description'.

Similarly to `error', `cerror' raises condition as an error, but
automatically binds the `continue' restart, which can be used either
with the `continue' function in the handler, or in the interactive
debugger.  The `continue-description' must be a string, describing
what will happen if `continue' restart is invoked.

# Examples

Convert `x` to positive value if it is negative:

``` fennel
(fn sqrt [x]
  (var x x)
  (when (< x 0)
    (cerror \"convert x to positive value\" :neg-sqrt)
    (set x (- x)))
  (math.sqrt x))

(handler-bind [:neg-sqrt (fn [] (continue))]
  (assert-eq 2 (sqrt -4)))
```"
    (assert-compile (= :string (type continue-description))
                    "continue-description must be a string"
                    continue-description)
    (assert-compile (not= 'nil condition-object)
                    "condition-object must not be nil"
                    condition-object)
    `(restart-case (let [{:raise raise#} (require ,impl)]
                     (raise# :error ,condition-object))
       (:continue [] ,continue-description nil)))

  (fn ignore-errors [...]
    "Ignore all conditions of type error.  If error condition was raised,
returns nil and the condition as multiple values.  If no error
conditions were raised, returns the resulting values normally.  Lua
errors can be handled with this macro.

# Examples

Condition of type error is ignored:

``` fennel
(local result [])

(local (res condition)
  (ignore-errors
    (table.insert result 1)
    (table.insert result 2)
    (error :some-error)
    (table.insert result 3)))

(assert-not res)
(assert-eq :some-error condition)
(assert-eq [1 2] result)
```"
    `(let [cs# (require ,impl)]
       (handler-case (do ,...)
         (cs#.Error [c#] (values nil c#)))))

  (fn unwind-protect [expr ...]
    "Runs `expr` in protected call, and runs all other forms as cleanup
forms before returning the value, whether `expr` returned normally or
an error occurred.  Similar to try/finally without a catch.

# Examples

``` fennel
(local result [])

(ignore-errors
  (unwind-protect
      (/ 1 nil)
    (table.insert result 1)
    (table.insert result 2)))

(assert-eq [1 2] result)
```"
    `(let [{:pack pack# :unpack unpack#} (require ,utils)]
       (let [(ok# res#) ,(if (vararg?)
                             `(pcall (fn [...] (pack# (do ,expr))) ...)
                             `(pcall (fn [] (pack# (do ,expr)))))]
         (if ok#
             (do (do ,...) (unpack# res#))
             (do (do ,...) (_G.error res#))))))

  (tset macro-loaded lib-name
        {: restart-case
         : handler-bind
         : handler-case
         : cerror
         : define-condition
         : ignore-errors
         : unwind-protect}))

(tset package.preload (.. lib-name :-utils)
      (or (. package.preload (.. lib-name :-utils))
          (fn [...]
;;;###include src/utils.fnl
            )))

(tset package.preload (.. lib-name :-debugger)
      (or (. package.preload (.. lib-name :-debugger))
          (fn [...]
;;;###include src/debugger.fnl
            )))

(tset package.preload (.. lib-name :-impl)
      (or (. package.preload (.. lib-name :-impl))
          (fn [...]
;;;###include src/impl.fnl
            )))

(local {: invoke-debugger}
  (require :condition-system-debugger))

(local {: pack : dynamic-scope}
  (require :condition-system-utils))

(local {: raise
        : invoke-restart
        : find-restart
        : Condition
        : Warning
        : Error
        : condition=}
  (require :condition-system-impl))

(fn error* [condition-object]
  "Raise `condition-object' as an error.

This function is a drop-in replacement for the inbuilt `error`
function.  Similarly to Lua's `error` accepting message as its first
argument, this function accepts condition object as it's first
argument, although it ignores the second argument, because the
throwing semantics are different.  Like Lua's `error`, this function
will interrupt function execution where it was called, and no code
after `error` will be executed.  If no handler bound for raised
condition, the condition will be promoted to a Lua error with detailed
message about the unhandled condition and it's arguments, if any.

```
>> (error :condition-object)
runtime error: condition condition-object was raised
stack traceback...
```

Condition objects support inheritance, and all conditions that are
raised with the `error' function automatically derive from `Error'
condition, and can be catched with handler bound to this condition
object.

Likewise all conditions automatically derive from `Condition'
condition, which is a base type for all condition objects.

Any Lua object can be a condition, and such conditions are handled by
reference, but still can be handled by binding handler for `Condition'
and `Error' (in case Lua object was raised with the `error` function).
If more complex inheritance rules are required, `define-condition' and
`make-condition' can be used.

# Examples

Condition is thrown as a Lua error if not handled, and can be caught
with `pcall`:

``` fennel
(assert-not (pcall error :error-condition))
```

Error conditions, and Lua errors can be handled by binding a handler
to `Error` and `Condition` conditions via `handler-case':

``` fennel
(assert-eq 27 (handler-case (error :some-error-condition)
                (Condition [] 27)))

(assert-eq 42 (handler-case (/ 1 nil)
                (Error [] 42)))
```

User-defined conditions can be handled by their base type:

``` fennel
(define-condition some-error)

(fn handle-condition []
  (handler-case (error (make-condition some-error 42))
    (some-error [_ x] x)))

(assert-eq 42 (handle-condition))
```

Conditions also can be recovered with `handler-bind' and
`restart-case' by using `invoke-restart'.  Error handling code doesn't
necessary has to be a part of the same lexical scope of where
condition was raised:

``` fennel
(define-condition some-error)

(fn some-function []
  (restart-case (error (make-condition some-error 32))
    (:use-value [x] x)))

(handler-bind [some-error
               (fn [_ x]
                 (invoke-restart :use-value (+ x 10)))]
  (assert-eq 42 (some-function)))
```

In this case, `restart-case' is in the lexical scope of
`some-function`, and `handler-bind' is outside of it's lexical
scope. When `some-function` raises `some-error` condition a handler
bound to this condition is executed. Handler invokes restart named
`:use-value`, which recovers function from error state and function
returns the value provided by the restart."
  (raise :error condition-object))

(fn signal [condition-object]
  "Raise `condition-object' as a signal.

Signals can be handled the same way as `error' conditions, but don't
promote to errors if no handler was found.  This function transfers
control flow to the handler at the point where it was called but will
continue execution if handler itself doesn't transfer control flow.

Signals derive from `Condition`, and can be catched with handler bound
to this type.

# Examples

Signal is ignored if not handled:

``` fennel
(assert-eq nil (signal :signal-condition))
```

Handler doesn't transfer control flow, and code evaluation continues
after `signal' call:

``` fennel
(local result [])

(handler-bind [:some-signal
               (fn [] (table.insert result 2))]
  (table.insert result 1)
  (signal :some-signal)
  (table.insert result 3))

(assert-eq [1 2 3] result)
```

See `error' for more examples on how to handle conditions."
  (raise :condition condition-object))

(fn warn [condition-object]
  "Raise `condition-object' as a warning.

Warnings are not thrown as errors when no handler is bound but the
message is printed to standard error out when warning condition is not
handled.  Similarly to `signal', the control is temporarily
transferred to a handler, but code evaluation continues if handler did
not transferred control flow.

Warnings derive from both `Warning` and `Condition`, and can be
catched by binding handler to any of these types.

# Examples

Warning is ignored if not handled:

```
(warn :warn-condition)
```

See `error' for examples how to handle conditions."
  (raise :warning condition-object))

(fn make-condition [condition-object arg1 ...]
  "Creates an instance of `condition-object'.  Accepts any amount of
additional arguments that will be passed as arguments to a handler
when handling this condition instance.

Condition created with `define-condition` and derived condition are
different objects, but the condition system sees those as the same
type.  Comparison semantics are such that derived condition is equal
to its base condition object.

# Examples

Defining a condition, making instance of this condition with two
arguments, and registering the handler for the original condition
object:

``` fennel
(define-condition some-condition)

(handler-case
    (error (make-condition some-condition {:foo \"bar\"} 42))
  (some-condition [c foo-bar forty-two]
    (assert-is (= some-condition c)) ; condition instance is equal to its base type
    (assert-eq {:foo \"bar\"} foo-bar)
    (assert-eq 42 forty-two)))
```"
  (assert (and (= :table (type condition-object))
               (= :condition condition-object.type)
               (not= nil condition-object.id))
          "condition must derive from existing condition object")
  (setmetatable
   {:data (if arg1 (pack arg1 ...))
    :id condition-object.id
    :parent condition-object.parent
    :type :condition}
   (getmetatable condition-object)))

(fn invoke-restart* [restart-name ...]
  "Invoke restart `restart-name' to handle a condition.

Additional arguments are passed to restart function as arguments.

Must be used only within the dynamic scope of `restart-case'.
Transfers control flow to restart function when executed.

# Examples

Handle the `error' with the `:use-value` restart:

``` fennel
(define-condition error-condition)

(fn handle-error []
  (handler-bind [error-condition
                 (fn [_c x]
                   (invoke-restart :use-value (+ x 10))
                   (print \"never prints\"))]
    (restart-case (do (error (make-condition error-condition 32))
                      (print \"also never prints\"))
      (:use-value [x] x))))

(assert-eq 42 (handle-error))
```

See `error' for examples how to handle conditions."
  (invoke-restart restart-name ...))

(fn invoke-debugger* [condition-object]
  "Invokes debugger for given `condition-object` to call restarts from
the interactive menu."
  (invoke-debugger condition-object))

(fn continue []
  "Invoke the `continue' restart, which is automatically bound by `cerror' macro.

Must be used only within the dynamic scope of `restart-case'.
Transfers control flow to handler function when executed."
  (invoke-restart :continue))

(fn find-restart* [restart-name]
  "Searches `restart-name' in the dynamic scope, and if found, returns
its name."
  (when (find-restart
         restart-name
         (?. dynamic-scope
             (or (and coroutine
                      coroutine.running
                      (tostring (coroutine.running)))
                 :main)
             :restarts))
    restart-name))

{:error error*
 : signal
 : warn
 : make-condition
 :find-restart find-restart*
 :invoke-restart invoke-restart*
 :invoke-debugger invoke-debugger*
 : continue
 : Condition
 : Warning
 : Error}
