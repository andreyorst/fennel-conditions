(local lua-error error)
(local lib-name (or ... :condition-system))

(eval-compiler
  (local lib-name (or ... :condition-system))
  (local impl (.. lib-name :-impl))
  (local utils (.. lib-name :-utils))

  ;; Utility functions

  (fn vararg? []
    ;; Check if ... is in in scope.
    (. (get-scope) :vararg))

  (fn current-scope []
    ;; A getter for current dynamic scope
    (let [_ (sym :_)
          scope (gensym :scope)]
      `(let [utils# (require ,utils)
             thread# (or (and coroutine
                              coroutine.running
                              (tostring (coroutine.running)))
                         :main)]
         (match (. utils# :dynamic-scope thread#)
           ,scope ,scope
           ,_ (let [,scope {:handlers {}
                            :restarts {}}]
                (tset utils# :dynamic-scope thread# ,scope)
                ,scope)))))

  (fn function-form? [form]
    ;; Check if `form` evaluates to a function
    (when (list? form)
      (let [[f name? arglist] form]
        (if (or (= 'fn f) (= 'lambda f) (= 'λ f))
            (if (sym? name?)
                (and (sequence? arglist) true)
                (and (sequence? name?) true))
            (= 'hashfn f)))))

  (fn seq-to-table [seq]
    ;; Transform binding sequence into associative table.
    ;; [:a 1 :b 2] => {:a 1 :b 2}
    (let [tbl {}]
      (for [i 1 seq.n 2]
        (tset tbl (. seq i) (. seq (+ i 1))))
      tbl))

  ;; Condition library public API macros

  (fn handler-bind [binding-vec ...]
    "Bind handlers to conditions.

`binding-vec' is a sequential table of conditions and their respecting
handlers, followed by the body expression.  Each handler should be a
function of at least one argument - the condition being handled.
Other arguments are optional, and can be used inside the handler.

If the body expression or any of its subsequent expressions raises a
condition, and a handler is bound for this condition type, the handler
function is invoked.  If no handler were bound, handlers are searched
up the dynamic scope. If no handler found, condition is thrown as a
Lua error.

If invoked handler exits normally, a condition is re-raised.  To
prevent re-raising, use `invoke-restart' function.

# Examples

Handlers executed but their return values are not used:

``` fennel
(var res nil)

(assert-not
 (handler-bind [:signal-condition (fn [] (set res \"signal\") 10)
                :error-condition (fn [] (set res \"error\") 20)]
   (signal :error-condition)))

(assert-eq res :error)
```

To provide a return value use either `handler-case' or a combination
of `restart-case' and `invoke-restart'."
    ;; This will be a common pattern for `handler-bind`, `restart-case`,
    ;; and `handler-case`.  A lot of stuff happens at compile time, and
    ;; dynamic scope is constructed at compile time to avoid explicit
    ;; iteration.  Because of it most variables are defined using
    ;; `gensym` to be accessible at compile time to build code, and at
    ;; runtime to access that code.
    (let [target (gensym :target)
          scope (gensym :scope)
          binding-len (length binding-vec)
          setup '(do)]
      (assert-compile (= (% binding-len 2) 0)
                      "expected even number of signal/handler bindings"
                      binding-vec)
      ;; check each handler to be a symbol or a function definition, and
      ;; put a handler into dynamic scope constructor stored in `setup`
      (for [i binding-len 1 -2]
        (let [condition-object (. binding-vec (- i 1))
              handler (. binding-vec i)]
          (assert-compile (or (sym? handler) (function-form? handler))
                          "handler must be a function"
                          handler)
          (table.insert setup `(assert (not= nil ,condition-object)
                                       "condition object must not be nil"))
          (table.insert
           setup
           `(tset ,scope :handlers {:parent (. ,scope :handlers)
                                    :target ,target
                                    :handler-type :handler-bind
                                    :handler {,condition-object
                                              ,handler}}))))
      `(let [,target {}
             {:pack pack#} (require ,utils)
             {:pcall-handler-bind pcall-handler-bind#} (require ,impl)
             ,scope ,(current-scope)
             orig-handlers# (. ,scope :handlers)]
         ,setup
         ,(if (vararg?)
              `(pcall-handler-bind#
                (fn [...] (pack# (do ,...))) ,scope ,target orig-handlers# ...)
              `(pcall-handler-bind#
                (fn [] (pack# (do ,...))) ,scope ,target orig-handlers#)))))

  (fn restart-case [expr ...]
    "Condition restart point.

Accepts expression `expr' and restarts that can be used when handling
conditions thrown from within the expression.  Similarly to
`handler-case' restarts are lists with first element being a restart
name, and an fn-tail.

If expression or any of it's subsequent expressions raises a
condition, it will be possible to return into the `restart-case', and
execute one of the provided restarts.  Restarts can be executed with
the `invoke-restart' function from handlers bound with `handler-bind'.
Restart names are always strings.

# Examples

Specifying two restarts for `:signal-condition`:

``` fennel
(restart-case (signal :signal-condition)
  (:some-restart [] :body)
  (:some-other-restart [] :other-body))
```"
    (let [target (gensym :target)
          scope (gensym :scope)
          restarts (table.pack ...)
          setup '(do)]
      (for [i restarts.n 1 -1]
        (let [[restart & [arglist &as fn-tail]] (. restarts i)]
          (assert-compile (list? (. restarts i)) "restarts must be defined as lists" restart)
          (assert-compile (or (sequence? arglist)) "expected parameter table" restart)
          (assert-compile (or (= :string (type restart))) "restart name must be a string literal" restart)
          (let [[args descr body] fn-tail
                restart {:restart (list 'fn (unpack fn-tail))
                         :name restart
                         :description (when (and (= :string (type descr))
                                                 (not= nil body))
                                        descr)
                         :args (when (not= nil (next args))
                                 (icollect [_ v (ipairs args)]
                                   (view v {:one-line? true})))}]
            (table.insert setup `(tset ,scope :restarts {:parent (. ,scope :restarts)
                                                         :target ,target
                                                         :restart ,restart})))))
      `(let [,target {}
             {:pack pack#} (require ,utils)
             {:pcall-restart-case pcall-restart-case#} (require ,impl)
             ,scope ,(current-scope)
             orig-restarts# (. ,scope :restarts)]
         ,setup
         ,(if (vararg?)
              `(pcall-restart-case#
                (fn [...] (pack# (do ,expr))) ,scope ,target orig-restarts# ...)
              `(pcall-restart-case#
                (fn [] (pack# (do ,expr))) ,scope ,target orig-restarts#)))))

  (fn handler-case [expr ...]
    "Condition handling point, similar to try/catch.

Accepts expression `expr' and handlers that can be used to handle
conditions, raised from within the expression or any subsequent
expressions.  Provides the facility to catch named conditions raised
with `signal', `warn` and `error' functions.  If any condition is
raised, before propagating condition to error, a handler is searched.
If handler is bound for this condition, it is executed, and the result
of `handler-case' expression will be the result of the handler.

Handlers are defined as lists, where the first object represents the
condition to handle, and the rest is fn-tail - a sequential table of
function arguments, followed by the function body.

# Examples

Handling `error' condition:

``` fennel
(assert-eq 42 (handler-case (error :error-condition)
                (:error-condition [] 42)))
```"
    (let [target (gensym :target)
          scope (gensym :scope)
          handlers (table.pack ...)
          setup `(do)]
      (for [i handlers.n 1 -1]
        (let [[condition-object arglist &as handler] (. handlers i)]
          (assert-compile (list? handler) "handlers must be defined as lists" handler)
          (assert-compile (sequence? arglist) "expected parameter table" handler)
          (table.insert setup `(assert (not= nil ,condition-object)
                                       "condition object must not be nil"))
          (table.insert setup `(tset ,scope :handlers {:parent (. ,scope :handlers)
                                                       :target ,target
                                                       :handler-type :handler-case
                                                       :handler {,condition-object ,(list 'fn (unpack handler 2))}}))))
      `(let [,target {}
             {:pack pack#} (require ,utils)
             {:pcall-handler-case pcall-handler-case#} (require ,impl)
             ,scope ,(current-scope)
             orig-handlers# (. ,scope :handlers)]
         ,setup
         ,(if (vararg?)
              `(pcall-handler-case#
                (fn [...] (pack# (do ,expr))) ,scope ,target orig-handlers# ...)
              `(pcall-handler-case#
                (fn [] (pack# (do ,expr))) ,scope ,target orig-handlers#)))))

  (fn define-condition [condition-symbol ...]
    "Create base condition object with `condition-symbol' from which
conditions can later be derived with `make-condition'.  Accepts
additional `:parent` and `:name` key value pairs.  If no `:name`
specified, uses `condition-symbol`'s `tostring` representation.  If no
`:parent` given uses `Condition' object as a parent.

# Examples

Altering condition's printable name:

``` fennel
(define-condition dbz :name :divide-by-zero)
```

Creating `math-error` condition with parent set to `Error` condition,
and `divide-by-zero` condition with parent set to `math-error`, and handling it:

``` fennel
(define-condition math-error :parent Error)
(define-condition divide-by-zero :parent math-error)

(assert-eq :ok (handler-case (error divide-by-zero)
                 (math-error [] :ok)))
```"
    (assert-compile (sym? condition-symbol) "condition-object must be a symbol" condition-symbol)
    (let [allowed-options {:parent true :name true}
          options (seq-to-table (table.pack ...))
          condition-object {:name (tostring condition-symbol) :type :condition}]
      (each [k (pairs options)]
        (assert-compile (. allowed-options k) (.. "invalid key: " (tostring k)) k))
      (each [k (pairs allowed-options)]
        (match (. options k)
          v (tset condition-object k v)))
      (when (= nil condition-object.parent)
        (tset condition-object :parent `(. (require ,impl) :Condition)))
      `(local ,condition-symbol (let [{:condition= eq#} (require ,impl)
                                      condition-object# ,condition-object
                                      name# condition-object#.name]
                                  (doto condition-object#
                                    (setmetatable {:__eq eq#
                                                   :__name (.. "condition " name#)
                                                   :__fennelview #(.. "#<" (tostring condition-object#) ">")})
                                    (tset :id condition-object#))))))

  (fn cerror [continue-description condition-object ...]
    "Raise `condition-object' as an error with auto-bound `:continue` restart, described by `continue-description'.

Similarly to `error', `cerror' raises condition as an error, but
automatically binds the `continue' restart, which can be used either
with the `continue' function in the handler, or in the interactive
debugger.  The `continue-description' must be a string, describing
what will happen if `continue' restart is invoked.

# Examples

Convert `x` to positive value if it is negative:

``` fennel
(fn sqrt [x]
  (var x x)
  (when (< x 0)
    (cerror \"convert x to positive value\" :neg-sqrt)
    (set x (- x)))
  (math.sqrt x))

(handler-bind [:neg-sqrt (fn [] (continue))]
  (assert-eq 2 (sqrt -4)))
```"
    (assert-compile (= :string (type continue-description))
                    "continue-description must be a string"
                    continue-description)
    (assert-compile (not= 'nil condition-object)
                    "condition-object must not be nil"
                    condition-object)
    `(restart-case (let [{:raise raise#} (require ,impl)]
                     (raise# :error ,condition-object))
       (:continue [] ,continue-description nil)))

  (fn ignore-errors [...]
    "Ignore all conditions of type error.  If error condition was raised,
returns nil and the condition as multiple values.  If no error
conditions were raised, returns the resulting values normally.  Lua
errors can be handled with this macro.

# Examples

Condition of type error is ignored:

``` fennel
(local result [])

(local (res condition)
  (ignore-errors
    (table.insert result 1)
    (table.insert result 2)
    (error :some-error)
    (table.insert result 3)))

(assert-not res)
(assert-eq :some-error condition)
(assert-eq [1 2] result)
```"
    `(let [cs# (require ,impl)]
       (handler-case (do ,...)
         (cs#.Error [c#] (values nil c#)))))

  (fn unwind-protect [expr ...]
    "Runs `expr` in protected call, and runs all other forms as cleanup
forms before returning the value, whether `expr` returned normally or
an error occurred.  Similar to try/finally without a catch.

# Examples

``` fennel
(local result [])

(ignore-errors
  (unwind-protect
      (/ 1 nil)
    (table.insert result 1)
    (table.insert result 2)))

(assert-eq [1 2] result)
```"
    `(let [{:pack pack# :unpack unpack#} (require ,utils)]
       (let [(ok# res#) ,(if (vararg?)
                             `(pcall (fn [...] (pack# (do ,expr))) ...)
                             `(pcall (fn [] (pack# (do ,expr)))))]
         (if ok#
             (do (do ,...) (unpack# res#))
             (do (do ,...) (_G.error res#))))))

  (tset macro-loaded lib-name
        {: restart-case
         : handler-bind
         : handler-case
         : cerror
         : define-condition
         : ignore-errors
         : unwind-protect}))

(tset package.preload (.. lib-name :-utils)
      (or (. package.preload (.. lib-name :-utils))
          (fn [...]
(local {: metadata : view} (require :fennel))

(fn current-thread []
  "Returns the name of current thread when possible"
  (or (and coroutine
           coroutine.running
           (tostring (coroutine.running)))
      :main))

(local dynamic-scope
  (metadata:set
   {}
   :fnl/docstring
   "Dynamic scope for the condition system.

Dynamic scope is a maintained table where handlers and restarts are
stored thread-locally.  Thread name is obtained with
`coroutine.running` call and each thread holds a table with the
following keys `:handlers`, `:restarts`, and `:current-context`.
Handlers and restarts itselves are tables."))

(fn get-name [condition-object]
  "Extracts name string from `condition-object'.

# Examples

Condition objects return base condition name:

``` fennel
(define-condition simple-error)
(assert-eq
 :simple-error
 (get-name (make-condition simple-error -1)))
```

Primitive objects are transformed with `tostring`:

``` fennel
(assert-eq \"-1\" (get-name -1))
```"
  (if (and (= :table (type condition-object))
           (= condition-object.type :condition))
      (tostring condition-object.id.name)
      (view condition-object)))

(fn get-data [condition-object]
  "Extracts data from `condition-object'.

# Examples

Extracting data set with `make-condition' function:

``` fennel
(define-condition simple-error)
(assert-eq
 {1 :a 2 :b :n 2}
 (get-data (make-condition simple-error :a :b)))
```

Primitive objects return table with `:n` set to 0:

``` fennel
(assert-eq
 {:n 0}
 (get-data :simple-condition))
```"
  (match (and (= :table (type condition-object))
              (= condition-object.type :condition)
              condition-object.data)
    (where data data) data
    _ {:n 0}))

(fn build-arg-str [sep args]
  "Constructs the string of arguments pretty-printed values stored in
`args', separated by `sep'."
  (let [res []]
    (for [i 1 args.n]
      (table.insert res (view (. args i) {:one-line? true})))
    (table.concat res sep)))

(fn compose-error-message [condition-object]
  "Composes message for `condition-object' based on it's name and data
stored within the object.

# Examples

Conditions without data produce short messages:

``` fennel
(define-condition simple-error)
(assert-eq
 \"condition simple-error was raised\"
 (compose-error-message simple-error))
```

Conditions with data produce extended messages:

``` fennel
(define-condition simple-error)
(assert-eq
 \"condition simple-error was raised with the following arguments: 1, 2, 3\"
 (compose-error-message (make-condition simple-error 1 2 3)))
```"
  (: "condition %s was raised%s" :format
     (get-name condition-object)
     (match (build-arg-str ", " (get-data condition-object))
       "" ""
       s (.. " with the following arguments: " s))))

(local unpack-fn (or table.unpack _G.unpack))
(fn unpack [tbl]
  "Automatically try to query `tbl` for it's size `n` and unpack whole
thing."
  (let [len (or tbl.n (length tbl))]
    (unpack-fn tbl 1 len)))

(fn pack [...]
  "Portable `table.pack` implementation."
  (doto [...]
    (tset :n (select :# ...))))

{: current-thread
 : dynamic-scope
 : get-data
 : get-name
 : compose-error-message
 : build-arg-str
 : unpack
 : pack}
            )))

(tset package.preload (.. lib-name :-debugger)
      (or (. package.preload (.. lib-name :-debugger))
          (fn [...]
(local {: eval} (require :fennel))
(local {: dynamic-scope
        : current-thread
        : get-data
        : get-name
        : compose-error-message
        : unpack
        : pack
        : build-arg-str}
  (require :condition-system-utils))

(fn flatten-restarts [restarts scope]
  (if scope
      (let [ordered []]
        (match scope.restart
          restart (table.insert restarts
                                (doto restart (tset :target scope.target))))
        (flatten-restarts restarts scope.parent))
      restarts))

(fn take-action [{: name : restart : args : target : builtin?}]
  (if builtin?
      (error {:builtin? true :data (restart)})
      args
      (do (io.stderr:write
           "Provide inputs for " name
           " (args: [" (table.concat args " ") "]) (^D to cancel)\n"
           "debugger:" name ">> ")
          (match (io.stdin:read "*l")
            input (let [args (pack (eval (.. "(values " input ")")))]
                    {:state :restarted
                     :restart #(restart (unpack args))
                     :target target})
            _ (do (io.stderr:write "\n") nil)))
      {:state :restarted
       : restart
       : target}))

(fn longest-name-lenght [length-fn restarts]
  ;; Computes the longest restart name lenght
  (var longest 0)
  (each [_ {: name} (ipairs restarts)]
    (let [len (length-fn name)]
      (when (> len longest)
        (set longest len))))
  longest)

(fn display-restart-prompt [restarts]
  ;; Prints restart prompt.  Unique restart names are shown inside
  ;; square brackets, and can be called by name.
  (let [slength (or (?. _G :utf8 :len) #(length $))
        max-name-width (longest-name-lenght slength restarts)
        max-number-width (-> restarts length tostring length)
        seen {}]
    (io.stderr:write "restarts (invokable by number or by name):\n")
    (each [i {: name : description} (ipairs restarts)]
      (let [uniq-name (when (not (. seen name))
                        (tset seen name true)
                        name)
            pad (string.rep " " (- max-name-width (slength (or uniq-name ""))))
            restart-name (if uniq-name
                             (.. "[" uniq-name pad "] ")
                             (.. pad "   "))
            number-pad (string.rep " " (+ 1 (- max-number-width (length (tostring i)))))]
        (io.stderr:write
         "  " i ":" number-pad restart-name
         (if description
             (description:gsub "\n" " ")
             name)
         "\n")))))

;; forward declaration
(var invoke-debugger nil)

(fn restart-menu [restarts prompt? level scope]
  ;; Interactive restart menu.  Restarts are displayed with the
  ;; following format: `number: [name] name-or-docstring`, where
  ;; `name` is a unique restart name in current menu.  Restarts can be
  ;; called either by their number or unique name.  If restart accepts
  ;; arguments, the second prompt will be displayed with a hint on
  ;; what arguments are accepted by the restart.  If error occurs
  ;; during input phase, second level of debugger is entered.
  (when prompt?
    (display-restart-prompt restarts))
  (io.stderr:write "debugger>> ")
  (let [named {}
        _ (each [_ {: name &as restart} (ipairs restarts)]
            (when (not (. named name))
              (tset named name restart)))
        input (io.stdin:read "*l")
        action (. restarts (tonumber input))
        named-action (. named input)]
    (if (or action named-action)
        (match (pcall take-action (or action named-action))
          (true nil) (restart-menu restarts nil level) ; no user input provided
          ;; Restart was found and no errors happened up to the restart call
          (true restart) (error restart)
          ;; some builtin restart was called
          (false {:builtin? true : data})
          (match data
            ;; exiting the nested debugger session
            {:cancel true} data
            ;; debugger invoked when no handlers were found, throwing error will land on the top level
            {:throw true : message} (if (?. dynamic-scope (current-thread) :restarts :parent)
                                        (error {:state :error : message})
                                        (error message 3)))
          ;; error happened during argument providing (likely). Entering nested debug session
          (false res) (match (invoke-debugger res (+ (or level 1) 1))
                        {:cancel true} (restart-menu restarts true level)))
        (do (if (= nil input)
                (io.stderr:write "\n")
                (io.stderr:write "Wrong action. Use number from 1 to " (length restarts)
                                 " or restart name.\n"))
            (restart-menu restarts nil level)))))

(fn invoke-debugger* [condition-object level]
  "Invokes interactive debugger for given `condition-object'.  Accepts
optional `level', indicating current debugger depth.

Restarts in the menu are ordered by their definition order and dynamic
scope depth.  Restarts can be called by their number in the menu or
the name in square brackets.  For example, if `restart-case` defines
two restarts `:a` and `:b` and outer `restart-case` bounds restarts
`:a` and `:c` the following menu will be printed:

```
Debugger was invoked on unhandled condition:
1: [a    ] a
2: [b    ] b
3:         a
4: [c    ] c
5: [throw] Throw condition as a Lua error
debugger>>
```

If restart function has a docstring, it is printed after the square
brackets.  If no docstring is found, restart name is printed.

If restart accepts any arguments, a second prompt will be entered when
restart is chosen from the menu.  Above the prompt a hint with
argument names will be displayed.  In this prompt arguments to the
restart are provided as expressions separated by space:

```
Provide inputs for some-restart (args: [some-arg other-arg]) (^D to cancel)
debugger:some-restart>> (+ 1 2 3) {:some :table}
```

Debugger doesn't know anything about the environment, or variables, so
in this prompt only fully realized values can be used.

If an error happens during restart call, debug level increases, and
new `cancel` restart is added to the menu, that allows returning to
previous debug level."
  (let [thread (current-thread)]
    (when (= nil (. dynamic-scope thread))
      (tset dynamic-scope thread {:handlers {}
                                  :restarts {}}))
    (let [restarts (flatten-restarts [] (. dynamic-scope thread :restarts))]
      (when level
        (table.insert restarts
                      {:name :cancel
                       :restart #{:cancel true}
                       :builtin? true
                       :description (.. "Return to level " (- level 1) " debugger")}))
      (table.insert restarts
                    {:name :throw
                     :builtin? true
                     :restart #{:throw true :message (compose-error-message condition-object)}
                     :description "Throw condition as a Lua error"})
      (io.stderr:write
       (if level
           (.. "Level " level " debugger")
           "Debugger")
       " was invoked on unhandled condition: "
       (get-name condition-object)
       (match (get-data condition-object)
         (where args (> args.n 0))
         (.. ", raised with the following arguments: "
             (build-arg-str ", " args))
         _ "")
       "\n")
      (restart-menu restarts true level))))

(set invoke-debugger invoke-debugger*)

{: invoke-debugger}
            )))

(tset package.preload (.. lib-name :-impl)
      (or (. package.preload (.. lib-name :-impl))
          (fn [...]
(local lua-error error) ; error is exported in init.fnl which may redefine global error function
(local {: metadata : view}
  (require :fennel))
(local {: dynamic-scope
        : current-thread
        : compose-error-message
        : get-data
        : get-name
        : build-arg-str
        : unpack
        : pack}
  (require :condition-system-utils))

(local {: invoke-debugger}
  (require :condition-system-debugger))


;;; Default condition objects

(local Condition
  {:name "condition"
   :type "condition"})
(tset Condition :id Condition)
(metadata:set
 Condition
 :fnl/docstring
 "Condition object that acts as a base for all conditions.")

(local Warning
  {:name "warning"
   :parent Condition
   :type "condition"})
(tset Warning :id Warning)
(metadata:set
 Warning
 :fnl/docstring
 "Condition object that acts as a base for all warning conditions.
Inherits `Condition'.")

(local Error
  {:name "error"
   :parent Condition
   :type "condition"})
(tset Error :id Error)
(metadata:set
 Error
 :fnl/docstring
 "Condition object that acts as a base for all error conditions.
Inherits `Condition'.")


;;; Handlers

(fn find-parent-handler [condition-object scope]
  ;; Searches handler for `condition-object` parent in current scope
  ;; `scope` only.
  (when condition-object
    (match (?. scope.handler (?. condition-object :id :parent :id))
      handler {: handler : scope}
      nil (find-parent-handler condition-object.parent scope))))

(fn find-object-handler [condition-object type* scope]
  ;; Searches the handler for the `condition-object` in dynamic scope
  ;; `scope`.  If no handler is found in the current scope, searches
  ;; for handlers of all condition object parents.  If no parent
  ;; handler found goes to upper scope.
  (when scope
    (let [h scope.handler]
      (match (or (?. h condition-object.id)
                 (?. h (match type*
                         :error Error
                         :warning Warning))
                 (?. h Condition))
        handler {: handler : scope}
        nil (match (find-parent-handler condition-object scope)
              parent-handler parent-handler
              nil (find-object-handler condition-object type* scope.parent))))))

(fn find-primitive-handler [condition-object type* scope]
  ;; Checks is object is present in dynamic scope and a handler is
  ;; bound to it.
  (when scope
    (let [h scope.handler]
      (match (or (?. h condition-object)
                 (?. h (match type*
                         :error Error
                         :warning Warning))
                 (?. h Condition))
        handler {: handler : scope}
        nil (find-primitive-handler condition-object type* scope.parent)))))

(fn find-handler [condition-object type* scope]
  "Finds the `condition-object' handler of `type*` in dynamic scope
`scope`.  If `condition-object' is a table with `type` key equal to
`:condition` searches handler based on condition object's inheritance.
If anything else, searches handler by object reference."
  (if (and (= :table (type condition-object))
           (= :condition condition-object.type))
      (find-object-handler condition-object type* scope)
      (find-primitive-handler condition-object type* scope)))

(fn handle [condition-object type* ?scope]
  "Handle the `condition-object' of `type*' and optional `?scope`.

Finds the `condition-object' handler in the dynamic scope.  If found,
calls the handler, and returns a table with `:state` set to
`:handled`, and `:handler` bound to an anonymous function that calls
the restart."
  (let [thread (current-thread)
        thread-scope (. dynamic-scope thread)]
    (match (find-handler
            condition-object
            type*
            (or ?scope thread-scope.handlers))
      {: handler : scope}
      (match scope.handler-type
        :handler-case {:state :handled
                       :handler #(handler condition-object (unpack (get-data condition-object)))
                       :target scope.target
                       :condition-object condition-object
                       :type type*}
        :handler-bind (do (tset thread-scope :current-context scope)
                          (handler condition-object (unpack (get-data condition-object)))
                          (handle condition-object type* scope.parent))
        _ {:state :error
           :message (.. "wrong handler-type: " (view _))
           :condition condition-object})
      _ {:state :error
         :message (.. "no handler bound for condition: "
                      (get-name condition-object))
         :condition condition-object})))


;;; Restarts

(fn find-restart [restart-name scope]
  "Searches `restart-name' in dynamic scope `scope`."
  (when scope
    (match (?. scope :restart)
      (where restart (= restart.name restart-name))
      (values restart.restart scope.target)
      _ (find-restart restart-name scope.parent))))

(fn invoke-restart [restart-name ...]
  "Searches for `restart-name' in the dynamic scope and invokes the
restart with given arguments.  Always throws error, as
`invoke-restart' must transfer control flow out of the handler.  If
restart is found, calls the restart function and returns a table with
`:state` set to `:restarted`, and `:restart` bound to the restart
function."
  (let [args (pack ...)
        thread-scope (. dynamic-scope (current-thread))]
    (lua-error (match (find-restart restart-name thread-scope.restarts)
                 (restart target) {:state :restarted
                                   :restart #(restart (unpack args))
                                   :target target}
                 _ (let [msg (.. "restart " (view restart-name) " is not found")]
                     (if thread-scope.current-context
                         {:state :error
                          :message msg}
                         msg))) 2)))


;;; Conditions

(fn raise-condition [condition-object type*]
  ;; Raises `condition-object' as a condition of given `type*`, or
  ;; `:condition` if `type*` is not specified.  Conditions of types
  ;; `:condition` and `:warn` do not interrupt program flow, but still
  ;; can be handled.
  (match (handle condition-object (or type* :condition))
    {:state :handled &as res} (lua-error res 2)
    {:state :restarted &as res} (lua-error res 2)
    _ nil))

(fn raise-warning [condition-object]
  ;; Raises `condition-object' as a warning.  If condition was not
  ;; handled, prints the warning message to stderr, and continues.
  (match (raise-condition condition-object :warning)
    nil (do (io.stderr:write "WARNING: "
                             (compose-error-message condition-object)
                             "\n")
            nil)))

(fn raise-error [condition-object]
  ;; The only raise that always throws it's result as an error when
  ;; condition was not handled, unless
  ;; `condition-system-use-debugger?' is not set to logical true.  If
  ;; `condition-system-use-debugger?' is `true`, invokes the
  ;; interactive debugger.
  (match (raise-condition condition-object :error)
    nil (if _G.condition-system-use-debugger?
            (invoke-debugger condition-object)
            (lua-error (compose-error-message condition-object) 2))))

(fn raise [condition-type condition-object]
  "Raises `condition-object' as a condition of `condition-type'.
`condition-object' must not be `nil'."
  (assert (not= nil condition-object)
          "condition must not be nil")
  ;; If condition was raided inside handler we need to unwind the
  ;; stack to the point where we were in the handler.  Each `handle`
  ;; invocation sets the `current-context` field, and this field is
  ;; cleared when we exit the handler.
  (let [thread (current-thread)
        thread-scope (do (when (not (. dynamic-scope thread))
                           (tset dynamic-scope thread {:handlers {} :restarts {}}))
                         (. dynamic-scope thread))]
    ;; unwind the dynamic stack if we're raising an error from a
    ;; handler, executed in the handler-bind context.
    (when (= thread-scope.handlers.handler-type :handler-bind)
      (match thread-scope.current-context
        scope (let [target scope.target]
                (var scope scope.parent)
                (while (and scope (= target scope.target))
                  (set scope scope.parent))
                (tset thread-scope :handlers scope))))
    (match condition-type
      :condition (raise-condition condition-object)
      :warning (raise-warning condition-object)
      :error (raise-error condition-object))))

(fn condition= [c1 c2]
  "Compare `c1` and `c2` condition objects by their `id` field."
  (and (= c1.type :condition)
       (= c2.type :condition)
       (rawequal c1.id c2.id)))

(fn pcall-handler-bind [f scope target orig-handlers ...]
  "Call `f` in given `scope` and pass result up to `target`.
Restore dynamic scope handlers to `orig-handlers`."
  (let [(ok res) (pcall f ...)]
    ;; Reset current scope context after exiting user code, but before
    ;; actually handling the condition in order to avoid infinity
    ;; loops.
    (doto scope
      (tset :current-context nil)
      (tset :handlers orig-handlers))
    (if ok (unpack res)
        (match res
          ;; Handled conditions are re-raised because `handler-bind`
          ;; requires exiting handler with `restart-case` or another
          ;; error.
          {:state :handled : target}
          (raise res.type res.condition-object)
          ;; Internal errors bubble up the dynamic scope until no
          ;; handlers and restarts left, to make sure we can't catch
          ;; those accidentally.
          {:state :error :message msg}
          (if (or scope.handlers.parent
                  scope.restarts.parent)
              (lua-error res)
              (lua-error msg 2))
          _ (lua-error res)))))

(fn pcall-restart-case [f scope target orig-restarts ...]
  "Call `f` in given `scope` and pass result up to `target`.
Restore dynamic scope restarts to `orig-restarts`."
  (let [restarts scope.restarts
        (ok res) (pcall f ...)]
    (doto scope
      (tset :restarts orig-restarts)
      (tset :current-context nil))
    (if ok (unpack res)
        (match res
          {:state :restarted :target target} (res.restart)
          {:state :restarted} (lua-error res)
          {:state :error :message msg}
          (if (or scope.handlers.parent
                  scope.restarts.parent)
              (lua-error res)
              (lua-error msg 2))
          ;; Encountered Lua error, we restore bound restarts, and
          ;; try to handle it as a condition.  Mostly repeats what
          ;; was done for a condition
          _ (let [(_ res2) (do (set scope.restarts restarts)
                               (pcall raise :error res))]
              (doto scope
                (tset :restarts orig-restarts)
                (tset :current-context nil))
              (match res2
                {:state :restarted : target} (res2.restart)
                {:state :restarted} (lua-error res2)
                {:state :error :message msg2}
                (if (or scope.handlers.parent
                        scope.restarts.parent)
                    (lua-error res2)
                    (lua-error msg2 2))
                ;; If Lua error was not handled, we throw it
                ;; instead of fail result from the handler
                _ (lua-error res)))))))

(fn pcall-handler-case [f scope target orig-handlers ...]
  "Call `f` in given `scope` and pass result up to `target`.
Restore dynamic scope handlers to `orig-handlers`."
  (let [handlers scope.handlers
        (ok res) (pcall f ...)]
    (doto scope
      (tset :handlers orig-handlers)
      (tset :current-context nil))
    (if ok (unpack res)
        (match res
          {:state :handled : target : handler} (handler)
          {:state :error : message}
          (if (or scope.handlers.parent
                  scope.restarts.parent)
              (lua-error res)
              (lua-error message 2))
          {:state :handled} (lua-error res)
          ;; In case Lua error was raised we reuse handlers defined
          ;; within this `handler-case` and up the dynamic scope.
          _ (match (find-handler res :error handlers)
              {:handler handler} (handler res)
              _ (lua-error res))))))

(setmetatable Condition {:__eq condition=
                         :__name "condition Condition"
                         :__fennelview #(.. "#<" (tostring Condition) ">")})
(setmetatable Warning {:__eq condition=
                       :__name "condition Warning"
                       :__fennelview #(.. "#<" (tostring Warning) ">")})
(setmetatable Error {:__eq condition=
                     :__name "condition Error"
                     :__fennelview #(.. "#<" (tostring Error) ">")})

{: raise
 : invoke-restart
 : handle
 : find-handler
 : find-restart
 : Condition
 : Error
 : Warning
 : condition=
 : pcall-handler-bind
 : pcall-restart-case
 : pcall-handler-case}
            )))

(local {: invoke-debugger}
  (require :condition-system-debugger))

(local {: pack : dynamic-scope}
  (require :condition-system-utils))

(local {: raise
        : invoke-restart
        : find-restart
        : Condition
        : Warning
        : Error
        : condition=}
  (require :condition-system-impl))

(fn error* [condition-object]
  "Raise `condition-object' as an error.

This function is a drop-in replacement for the inbuilt `error`
function.  Similarly to Lua's `error` accepting message as its first
argument, this function accepts condition object as it's first
argument, although it ignores the second argument, because the
throwing semantics are different.  Like Lua's `error`, this function
will interrupt function execution where it was called, and no code
after `error` will be executed.  If no handler bound for raised
condition, the condition will be promoted to a Lua error with detailed
message about the unhandled condition and it's arguments, if any.

```
>> (error :condition-object)
runtime error: condition condition-object was raised
stack traceback...
```

Condition objects support inheritance, and all conditions that are
raised with the `error' function automatically derive from `Error'
condition, and can be catched with handler bound to this condition
object.

Likewise all conditions automatically derive from `Condition'
condition, which is a base type for all condition objects.

Any Lua object can be a condition, and such conditions are handled by
reference, but still can be handled by binding handler for `Condition'
and `Error' (in case Lua object was raised with the `error` function).
If more complex inheritance rules are required, `define-condition' and
`make-condition' can be used.

# Examples

Condition is thrown as a Lua error if not handled, and can be caught
with `pcall`:

``` fennel
(assert-not (pcall error :error-condition))
```

Error conditions, and Lua errors can be handled by binding a handler
to `Error` and `Condition` conditions via `handler-case':

``` fennel
(assert-eq 27 (handler-case (error :some-error-condition)
                (Condition [] 27)))

(assert-eq 42 (handler-case (/ 1 nil)
                (Error [] 42)))
```

User-defined conditions can be handled by their base type:

``` fennel
(define-condition some-error)

(fn handle-condition []
  (handler-case (error (make-condition some-error 42))
    (some-error [_ x] x)))

(assert-eq 42 (handle-condition))
```

Conditions also can be recovered with `handler-bind' and
`restart-case' by using `invoke-restart'.  Error handling code doesn't
necessary has to be a part of the same lexical scope of where
condition was raised:

``` fennel
(define-condition some-error)

(fn some-function []
  (restart-case (error (make-condition some-error 32))
    (:use-value [x] x)))

(handler-bind [some-error
               (fn [_ x]
                 (invoke-restart :use-value (+ x 10)))]
  (assert-eq 42 (some-function)))
```

In this case, `restart-case' is in the lexical scope of
`some-function`, and `handler-bind' is outside of it's lexical
scope. When `some-function` raises `some-error` condition a handler
bound to this condition is executed. Handler invokes restart named
`:use-value`, which recovers function from error state and function
returns the value provided by the restart."
  (raise :error condition-object))

(fn signal [condition-object]
  "Raise `condition-object' as a signal.

Signals can be handled the same way as `error' conditions, but don't
promote to errors if no handler was found.  This function transfers
control flow to the handler at the point where it was called but will
continue execution if handler itself doesn't transfer control flow.

Signals derive from `Condition`, and can be catched with handler bound
to this type.

# Examples

Signal is ignored if not handled:

``` fennel
(assert-eq nil (signal :signal-condition))
```

Handler doesn't transfer control flow, and code evaluation continues
after `signal' call:

``` fennel
(local result [])

(handler-bind [:some-signal
               (fn [] (table.insert result 2))]
  (table.insert result 1)
  (signal :some-signal)
  (table.insert result 3))

(assert-eq [1 2 3] result)
```

See `error' for more examples on how to handle conditions."
  (raise :condition condition-object))

(fn warn [condition-object]
  "Raise `condition-object' as a warning.

Warnings are not thrown as errors when no handler is bound but the
message is printed to standard error out when warning condition is not
handled.  Similarly to `signal', the control is temporarily
transferred to a handler, but code evaluation continues if handler did
not transferred control flow.

Warnings derive from both `Warning` and `Condition`, and can be
catched by binding handler to any of these types.

# Examples

Warning is ignored if not handled:

```
(warn :warn-condition)
```

See `error' for examples how to handle conditions."
  (raise :warning condition-object))

(fn make-condition [condition-object arg1 ...]
  "Creates an instance of `condition-object'.  Accepts any amount of
additional arguments that will be passed as arguments to a handler
when handling this condition instance.

Condition created with `define-condition` and derived condition are
different objects, but the condition system sees those as the same
type.  Comparison semantics are such that derived condition is equal
to its base condition object.

# Examples

Defining a condition, making instance of this condition with two
arguments, and registering the handler for the original condition
object:

``` fennel
(define-condition some-condition)

(handler-case
    (error (make-condition some-condition {:foo \"bar\"} 42))
  (some-condition [c foo-bar forty-two]
    (assert-is (= some-condition c)) ; condition instance is equal to its base type
    (assert-eq {:foo \"bar\"} foo-bar)
    (assert-eq 42 forty-two)))
```"
  (assert (and (= :table (type condition-object))
               (= :condition condition-object.type)
               (not= nil condition-object.id))
          "condition must derive from existing condition object")
  (setmetatable
   {:data (if arg1 (pack arg1 ...))
    :id condition-object.id
    :parent condition-object.parent
    :type :condition}
   (getmetatable condition-object)))

(fn invoke-restart* [restart-name ...]
  "Invoke restart `restart-name' to handle a condition.

Additional arguments are passed to restart function as arguments.

Must be used only within the dynamic scope of `restart-case'.
Transfers control flow to restart function when executed.

# Examples

Handle the `error' with the `:use-value` restart:

``` fennel
(define-condition error-condition)

(fn handle-error []
  (handler-bind [error-condition
                 (fn [_c x]
                   (invoke-restart :use-value (+ x 10))
                   (print \"never prints\"))]
    (restart-case (do (error (make-condition error-condition 32))
                      (print \"also never prints\"))
      (:use-value [x] x))))

(assert-eq 42 (handle-error))
```

See `error' for examples how to handle conditions."
  (invoke-restart restart-name ...))

(fn invoke-debugger* [condition-object]
  "Invokes debugger for given `condition-object` to call restarts from
the interactive menu."
  (invoke-debugger condition-object))

(fn continue []
  "Invoke the `continue' restart, which is automatically bound by `cerror' macro.

Must be used only within the dynamic scope of `restart-case'.
Transfers control flow to handler function when executed."
  (invoke-restart :continue))

(fn find-restart* [restart-name]
  "Searches `restart-name' in the dynamic scope, and if found, returns
its name."
  (when (find-restart
         restart-name
         (?. dynamic-scope
             (or (and coroutine
                      coroutine.running
                      (tostring (coroutine.running)))
                 :main)
             :restarts))
    restart-name))

{:error error*
 : signal
 : warn
 : make-condition
 :find-restart find-restart*
 :invoke-restart invoke-restart*
 :invoke-debugger invoke-debugger*
 : continue
 : Condition
 : Warning
 : Error}
