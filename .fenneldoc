;; -*- mode: fennel; -*- vi:ft=fennel
;; Configuration file for Fenneldoc v0.1.5
;; https://gitlab.com/andreyorst/fenneldoc

{:fennel-path {}
 :function-signatures true
 :ignored-args-patterns ["[a-z]" "%.%.%." "arg1"]
 :inline-references "link"
 :insert-comment true
 :insert-copyright true
 :insert-license true
 :insert-version true
 :mode "checkdoc"
 :order "alphabetic"
 :out-dir "./doc"
 :project-copyright "Copyright (C) 2021 Andrey Listopadov"
 :modules-info {:condition-system.fnl
                {:description
                 "Condition system for the Fennel language.

This module provides a set of functions and macros for control transfer, that implement Common Lisp-inspired condition system for the Fennel language."
                 :doc-order ["handler-case"
                             "handler-bind"
                             "restart-case"
                             "cerror"
                             "error"
                             "warn"
                             "signal"
                             "invoke-restart"
                             "continue"]}}
 :project-license "[MIT](https://gitlab.com/andreyorst/fennel-conditions/-/raw/master/LICENSE)"
 :project-version "v0.2.0"
 :sandbox false
 :test-requirements {:condition-system.fnl
                     "(require-macros (doto :fennel-test.fennel-test require))
                      (require-macros (doto :condition-system require))
                      (local {: signal : continue : error : make-condition : Error} (require :condition-system))"}
 :toc true}
