(local {: error} (require :condition-system))
(require-macros (doto :condition-system require))

(fn foo []
  (restart-case (error :error)
    (:r [] :ok)))

{: foo}
