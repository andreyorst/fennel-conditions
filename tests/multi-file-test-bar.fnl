(require-macros (doto :fennel-test.fennel-test require))
(import-macros {: restart-case : handler-bind : handler-case} (doto :condition-system require))
(local {: error : signal : invoke-restart} (require :condition-system))
(local {: foo} (require :tests.multi-file-test-foo))

(deftest handling-from-enother-module
  (testing "calling restart from another module"
    (handler-bind [:error (fn [] (invoke-restart :r))]
      (assert-eq :ok (foo)))))
